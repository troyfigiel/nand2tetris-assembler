import io

import pytest
from pydantic import ValidationError

from assembler.preprocessing import preprocess
from assembler.symbols import SymbolTable


@pytest.mark.parametrize(
    ("input_program", "expected_instruction_lines", "expected_label_symbols"),
    (
        ("", [], SymbolTable({})),
        ("    @10\n    M=1", ["@10", "M=1"], SymbolTable({})),
        ("    @10\n    M=1\n    \n", ["@10", "M=1"], SymbolTable({})),
        ("@10\n// Comment\nM=1", ["@10", "M=1"], SymbolTable({})),
        ("    @10\n    // Comment\n    M=1", ["@10", "M=1"], SymbolTable({})),
        ("@10 // Inline comment\nM=1", ["@10", "M=1"], SymbolTable({})),
        ("(START)\n@10\nM=1", ["@10", "M=1"], SymbolTable({"START": 0})),
        (
            "(START)\n@10\nM=1\n(END)\n@2\nD=M",
            ["@10", "M=1", "@2", "D=M"],
            SymbolTable({"START": 0, "END": 2}),
        ),
    ),
)
def test_preprocess(
    input_program,
    expected_instruction_lines,
    expected_label_symbols,
):
    actual_instruction_lines, actual_label_symbols = preprocess(
        io.StringIO(input_program),
    )

    assert actual_instruction_lines == expected_instruction_lines
    assert actual_label_symbols == expected_label_symbols


@pytest.mark.parametrize(
    "input_program",
    ("(10)\n@10\nM=1",),
)
def test_preprocess_invalid_lines(input_program):
    with pytest.raises(ValidationError):
        preprocess(io.StringIO(input_program))
