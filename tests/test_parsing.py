import pytest

from assembler.parsing import AInstruction, CInstruction, parse
from assembler.symbols import SymbolTable


@pytest.mark.parametrize(
    ("input_lines", "input_symbol_table", "expected_instructions"),
    (
        ([], SymbolTable({}), []),
        (["@20"], SymbolTable({}), [AInstruction(20)]),
        (["@i"], SymbolTable({}), [AInstruction(16)]),
        (["@START"], SymbolTable({"START": 1}), [AInstruction(1)]),
        (
            ["@20", "M=A;JEQ"],
            SymbolTable({}),
            [AInstruction(20), CInstruction("M", "A", "JEQ")],
        ),
        (
            ["@i", "M=A", "@j", "D=A"],
            SymbolTable({}),
            [
                AInstruction(16),
                CInstruction("M", "A", "NULL"),
                AInstruction(17),
                CInstruction("D", "A", "NULL"),
            ],
        ),
        (
            ["@i", "M=0", "@j", "M=A", "@i", "0;JEQ"],
            SymbolTable({}),
            [
                AInstruction(16),
                CInstruction("M", "0", "NULL"),
                AInstruction(17),
                CInstruction("M", "A", "NULL"),
                AInstruction(16),
                CInstruction("NULL", "0", "JEQ"),
            ],
        ),
    ),
)
def test_parse_valid_instructions(
    input_lines,
    input_symbol_table,
    expected_instructions,
):
    actual_instructions = list(parse(input_lines, input_symbol_table))

    assert actual_instructions == expected_instructions


@pytest.mark.parametrize(
    ("input_lines", "input_symbol_table"),
    ((["@32768"], SymbolTable({})), (["K=A"], SymbolTable({}))),
)
def test_parse_invalid_instructions(input_lines, input_symbol_table):
    with pytest.raises(ValueError, match=f"^Could not parse line {input_lines[0]}$"):
        list(parse(input_lines, input_symbol_table))
