import pathlib

import pytest

import assembler


@pytest.fixture()
def asm_dir(tmp_path):
    asm_dir = tmp_path / "contains_asm"
    asm_dir.mkdir()
    return asm_dir


@pytest.fixture()
def asm_file(asm_dir):
    return asm_dir / "file.asm"


@pytest.fixture()
def hack_file(asm_dir):
    return asm_dir / "file.hack"


@pytest.fixture(
    params=[
        "Add",
        "Max",
        "MaxL",
        "Rect",
        "RectL",
        "Pong",
        "PongL",
    ],
)
def fixture_contents(asm_dir, request):
    fixtures_dir = pathlib.PosixPath("tests") / "fixtures"
    asm_content = (fixtures_dir / f"{request.param}.asm").read_text()
    hack_content = (fixtures_dir / f"{request.param}.hack").read_text()
    return asm_content, hack_content


@pytest.fixture()
def fixture_dir():
    return pathlib.PosixPath("tests") / "fixtures"


def test_assemble_empty_file(asm_file, hack_file):
    asm_file.write_text("\n")

    assembler.call(asm_file.as_posix())

    assert hack_file.read_text() == "\n"


def test_assemble_fixtures(asm_file, hack_file, fixture_contents):
    asm_content, hack_content = fixture_contents
    asm_file.write_text(asm_content)

    assembler.call(asm_file.as_posix())

    assert hack_file.read_text() == hack_content
