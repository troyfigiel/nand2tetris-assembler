import pytest
from pydantic import ValidationError

from assembler.symbols import SymbolTable


@pytest.mark.parametrize("input_table", ({"foo": -1}, {"foo": "bar"}))
def test_invalid_symbol_table(input_table):
    with pytest.raises(ValidationError):
        SymbolTable(input_table)
