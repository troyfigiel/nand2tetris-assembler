import pathlib
import sys
from typing import TextIO

from assembler.parsing import parse
from assembler.preprocessing import preprocess
from assembler.symbols import BUILTIN_SYMBOLS, SymbolTable


def assemble(file_object: TextIO) -> str:
    with file_object as f:
        instruction_lines, label_symbols = preprocess(f)

    instructions = parse(
        instruction_lines,
        SymbolTable({**label_symbols.table, **BUILTIN_SYMBOLS.table}),
    )
    binary_instructions = "\n".join(i.to_binary() for i in instructions)

    return f"{binary_instructions}\n"


def call(file_name: str | None = None) -> None:
    if file_name is None:
        file_name = sys.argv[1]

    asm_file = pathlib.Path(file_name)

    with asm_file.open() as f:
        hack_content = assemble(f)

    asm_file.with_suffix(".hack").write_text(hack_content)
