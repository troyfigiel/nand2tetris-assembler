import re
from collections.abc import Iterable
from typing import Protocol

from aenum import MultiValueEnum
from pydantic import Field, ValidationError
from pydantic.dataclasses import dataclass

from assembler.symbols import START_DEFINED_SYMBOL_ADDRESS, SymbolTable

A_INSTRUCTION_REGEXP = "^@(?P<address>.+)$"
C_INSTRUCTION_REGEXP = "^(?:(?P<dest>.+)=)?(?P<comp>.+?)(?:;(?P<jump>.+))?$"


class Instruction(Protocol):
    def to_binary(self):
        ...


@dataclass
class AInstruction:
    address: int = Field(ge=0, lt=32768)

    def to_binary(self):
        return f"{int(self.address):016b}"


class Dest(MultiValueEnum):
    _init_ = "value binary"

    NULL = "NULL", 0b000
    M = "M", 0b001
    D = "D", 0b010
    MD = "MD", 0b011
    A = "A", 0b100
    AM = "AM", 0b101
    AD = "AD", 0b110
    ADM = "ADM", 0b111


class Comp(MultiValueEnum):
    _init_ = "value binary"

    ZERO = "0", 0b0101010
    ONE = "1", 0b0111111
    NEGATIVE_ONE = "-1", 0b0111010
    D = "D", 0b0001100
    A = "A", 0b0110000
    NOT_D = "!D", 0b0001101
    NOT_A = "!A", 0b0110001
    NEGATIVE_D = "-D", 0b0001111
    NEGATIVE_A = "-A", 0b0110011
    D_PLUS_ONE = "D+1", 0b0011111
    A_PLUS_ONE = "A+1", 0b0110111
    D_MINUS_ONE = "D-1", 0b0001110
    A_MINUS_ONE = "A-1", 0b0110010
    D_PLUS_A = "D+A", 0b0000010
    D_MINUS_A = "D-A", 0b0010011
    A_MINUS_D = "A-D", 0b0000111
    D_AND_A = "D&A", 0b0000000
    D_OR_A = "D|A", 0b0010101
    M = "M", 0b1110000
    NOT_M = "!M", 0b1110001
    NEGATIVE_M = "-M", 0b1110011
    M_PLUS_ONE = "M+1", 0b1110111
    M_MINUS_ONE = "M-1", 0b1110010
    D_PLUS_M = "D+M", 0b1000010
    D_MINUS_M = "D-M", 0b1010011
    M_MINUS_D = "M-D", 0b1000111
    D_AND_M = "D&M", 0b1000000
    D_OR_M = "D|M", 0b1010101


class Jump(MultiValueEnum):
    _init_ = "value binary"

    NULL = "NULL", 0b000
    JGT = "JGT", 0b001
    JEQ = "JEQ", 0b010
    JGE = "JGE", 0b011
    JLT = "JLT", 0b100
    JNE = "JNE", 0b101
    JLE = "JLE", 0b110
    JMP = "JMP", 0b111


@dataclass
class CInstruction:
    dest: Dest
    comp: Comp
    jump: Jump

    def to_binary(self):
        return (
            "111"
            f"{self.comp.binary:07b}"
            f"{self.dest.binary:03b}"
            f"{self.jump.binary:03b}"
        )


Line = str


def parse(lines: Iterable[Line], symbol_table: SymbolTable) -> Iterable[Instruction]:
    start_symbol_address = START_DEFINED_SYMBOL_ADDRESS
    table = symbol_table.table

    for line in lines:
        match = re.match(A_INSTRUCTION_REGEXP, line)
        if match is not None:
            address = match.groupdict()["address"]

            if not address.isnumeric():
                try:
                    address = table[address]
                except KeyError:
                    table[address] = start_symbol_address
                    address = start_symbol_address
                    start_symbol_address += 1

            try:
                yield AInstruction(address)
            except ValidationError:
                raise ValueError(f"Could not parse line {line}")

            continue

        match = re.match(C_INSTRUCTION_REGEXP, line)
        if match is not None:
            kwargs = match.groupdict()

            if kwargs["dest"] is None:
                kwargs["dest"] = "NULL"

            if kwargs["jump"] is None:
                kwargs["jump"] = "NULL"

            try:
                yield CInstruction(**kwargs)
            except ValidationError:
                raise ValueError(f"Could not parse line {line}")
