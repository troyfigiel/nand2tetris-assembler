import re
from typing import TextIO

from pydantic import StrictStr, validator
from pydantic.dataclasses import dataclass

from assembler.symbols import SymbolTable

COMMENT_START = "//"
L_INSTRUCTION_REGEXP = r"^\((?P<symbol>.+)\)$"


@dataclass
class LInstruction:
    symbol: StrictStr

    @validator("symbol")
    def is_not_numeric(cls, v: str):
        assert not v.isnumeric()
        return v


Line = str


def preprocess(file_object: TextIO) -> tuple[list[Line], SymbolTable]:
    instruction_lines = []
    label_symbols = {}
    line_number = 0

    with file_object as f:
        for line in f:
            if COMMENT_START in line:
                line = line[: line.find(COMMENT_START)]

            line = line.strip()
            if not line:
                continue

            match = re.match(L_INSTRUCTION_REGEXP, line)
            if match is not None:
                instruction = LInstruction(match.groupdict()["symbol"])
                label_symbols[instruction.symbol] = line_number
                continue

            instruction_lines.append(line)
            line_number += 1

    return instruction_lines, SymbolTable(label_symbols)
